import * as React from 'react';
// import {Ionicons} from '@expo/vector-icons';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ContextScreen from './app/Screens/ContextScreen';
import TodoListScreen from './app/Screens/TodoListScreen';
import {Text, View, Image} from 'react-native';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            if (route.name === 'Context') {
              return (
                <View>
                  <Image
                    style={{width: 30, height: 20}}
                    source={require('./icons/ic_navigation.png')}
                  />
                </View>
              );
            } else if (route.name === 'Todo') {
              return (
                <View style={{width: 20, height: 20}}>
                  <Image
                    style={{width: 30, height: 20}}
                    source={require('./icons/ic_totolist.png')}
                  />
                </View>
              );
            }
          },
          tabBarInactiveTintColor: 'gray',
          tabBarActiveTintColor: 'tomato',
        })}>
        <Tab.Screen name="Context" component={ContextScreen} />
        <Tab.Screen name="Todo" component={TodoListScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
