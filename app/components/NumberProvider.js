import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
const Context = React.createContext();

export default class NumberProvider extends Component {
  constructor(props) {
    super();
    this.state = {
      number: 5,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Context.Provider value={this.state.number}>
          <Text style={styles.child}>Context APi React-Native</Text>
          <TouchableOpacity
            onPress={() => this.setState({number: Math.random()})}
            style={styles.changeData}>
            <Text style={styles.textChange}>Change value</Text>
          </TouchableOpacity>
          <Child />
        </Context.Provider>
      </View>
    );
  }
}

class Child extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <View style={styles.container}>
        <Context.Consumer>
          {data => <Text style={styles.child}>Data is here: {data} </Text>}
        </Context.Consumer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 25,
    textAlign: 'center',
    margin: 10,
  },
  child: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  changeData: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textChange: {
    color: '#fff',
    backgroundColor: '#04A844',
    borderRadius: 25,
    paddingHorizontal: 130,
    paddingVertical: 10,
    fontWeight: 'bold',
  },
});
