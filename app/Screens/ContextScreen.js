import * as React from 'react';
import {Text, View} from 'react-native';
import NumberProvider from '../components/NumberProvider';

export default function SettingsScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <NumberProvider />
    </View>
  );
}
